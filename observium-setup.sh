#!/bin/bash
## Obserivum Client Setup Script
## v.0.5 - 12/15/13 - nunim@sonicboxes.com
## Tested on Debian 6/7 & Ubuntu 12.04+ - CentOS 5.8/6.4
## Useage: ./observium-setup.sh <Community> <Contact Email>
## check if root
if [ $(whoami) != "root" ]; then
        echo "You need to run this script as root."
        echo "Use 'sudo ./observium-setup.sh' then enter your password when prompted."
        exit 1
fi
## set community
COMMUNITY=$1
## set contact email
CONTACT=$2
## set hostname
#HOSTNAME=$(hostname -f)
## set distro
DISTRO=`cat /etc/*-release | grep -m 1 CentOS | awk {'print $1}'`
## check if community set
if [ -z "$COMMUNITY" ] ; then
echo "Community is not set"
echo "Please run the script again with ./observium-setup.sh <Community> <Contact Email>"
exit
fi
## check if contact email is set
if [ -z "$CONTACT" ] ; then
echo "Contact Email is not set"
echo "Please run the script again with ./observium-setup.sh <Community> <Contact Email>"
exit
fi
## set server location
read -p "Please enter where the server is physically located: " LOCATION
## check distro
if [ "$DISTRO" = "CentOS" ] ; then
	# clear yum cache
	yum clean all
	# install snmp daemon
	yum -y install net-snmp
	# set SNMP Daemon options
	sed -i.bak '/OPTIONS=/c\OPTIONS="-Lsd -Lf /dev/null -p /var/run/snmpd.pid"' /etc/sysconfig/snmpd
else
	# update package list
	apt-get update
	# install snmp daemon
	apt-get -y install snmpd
	# setup daemon options
	sed -i.bak "/SNMPDOPTS=/c\SNMPDOPTS='-Lsd -Lf /dev/null -u snmp -p /var/run/snmpd.pid'" /etc/default/snmpd
fi
#setup /etc/snmp/snmpd.conf
cat > /etc/snmp/snmpd.conf <<END
com2sec readonly 62.210.69.50 $COMMUNITY
group MyROGroup v1			readonly
group MyROGroup v2c        	readonly
group MyROGroup usm        	readonly
view all included  .1 80
access MyROGroup ""      any       noauth    exact  all    none   none
syslocation "${LOCATION}"
syscontact "${CONTACT}"
#This line allows Observium to detect the host OS if the distro script is installed
extend .1.3.6.1.4.1.2021.7890.1 distro /usr/bin/distro
END
#get distro checking script
wget http://www.observium.org/svn/observer/trunk/scripts/distro
mv distro /usr/bin/distro
chmod 755 /usr/bin/distro
#restart snmpd
/etc/init.d/snmpd restart
/bin/systemctl restart snmpd.service
#finished
clear
echo "#########################################################"
echo "##           !! !! Installation Complete !! !!         ##"
echo "#########################################################"
echo "#You may add this server to your Observium installation #"
echo "#          using $COMMUNITY as the Community            #"
echo "#########################################################"
echo "##         Install Script by www.SonicBoxes.com        ##"
echo "#########################################################"
